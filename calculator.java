import java.util.*;

class calculator
{
	public static void main(String ar[])
	{
	    Scanner sc= new Scanner(System.in);
		double a;
		System.out.println("Enter the First operand ");
		a=sc.nextDouble();
		double b;
		System.out.println("Enter the Second operand ");
		b=sc.nextDouble();
		int op;
		System.out.println("Choose the operation you want to perform from the following :- \n 1.Addition(+) \n 2.Subtraction(-) \n 3.Multiplication(*) \n 4.Division(/) ");
		op=sc.nextInt();
		double result;
        switch(op)
		{
			case 1:   System.out.println("Result is "+(a+b));
			          break;
			case 2:   System.out.println("Result is "+(a-b));
			          break;
			case 3:   System.out.println("Result is "+(a*b));
			          break;
			case 4:   System.out.println("Result is "+(a/b));
			          break;
		    default :  System.out.println("Invalid operation");
		}
		
	}
}